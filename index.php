<?php
	require_once 'lang.inc.php';
	require_once 'DB.class.php';
	require_once 'fctAux.inc.php';
	
	$db = new DB();
	$prefix = 'reurl_';
	
	$explodArr = explode('/', $_SERVER['REQUEST_URI']);
	
	// The last element of the array can be an id
	$id = $explodArr[count($explodArr)-1];
	
	// Used to print errors to user
	$errMess = '';
	$urlMini = '';

	// To go to a short URL
	if($id != basename($_SERVER['PHP_SELF']) && !empty($id)) {
		$paramArr = array('id' => $id);
		$location = $db->select('SELECT urlOrg FROM ' . $prefix . 'urls WHERE idURL = :id;', $paramArr);
		if(count($location) > 0) {
			$db->maj('UPDATE ' . $prefix . 'urls SET counter = counter + 1 WHERE idURL = :id;', $paramArr);

			// Redirecting
			header('Location:'.$location[0]['urlOrg']);
			exit();
		} else {
			$errMess = THIS_LINK_ISNT_IN_DATABASE;
		}
	}

	// To create a short URL
	if(isset($_POST['btnShort'])) {
		if(filter_var($_POST['txtUrlOrg'], FILTER_VALIDATE_URL) && substr_count($_POST['txtUrlOrg'], ADRESS) == 0) {
			$urlExist = $db->select('SELECT idURL FROM ' . $prefix . 'urls WHERE urlOrg = :txtUrlOrg;', array('txtUrlOrg' => $_POST['txtUrlOrg']));
			// If the URL is already stored in database, we don't need to create a new one
			if(count($urlExist) > 0) {
				$urlMini = $urlExist[0]['idURL'];
			} else {
				if (@fclose(@fopen($_POST['txtUrlOrg'], "r"))) {
					$urlMini = base_convert(uniqid(), 10, 36);
					$paramArr = array(
									'urlMini' => $urlMini,
									'txtUrlOrg' => $_POST['txtUrlOrg'],
									'ip' => $_SERVER['REMOTE_ADDR']
								);
					$db->maj('INSERT INTO ' . $prefix . 'urls(idURL, urlOrg, addDate, ipOrg) VALUES(:urlMini, :txtUrlOrg, now(), :ip);',
							$paramArr);
				} else {
					$errMess = INVALID_ADRESS;
				}
			}
		} else {
			$errMess = INVALID_ADRESS;
		}
	}

	echo showHeader();
?>
		<form method="POST" action="index.php">
			<input type="text" name="txtUrlOrg" id="txtUrlOrg"
			value='<?php echo (empty($errMess) && isset($_POST['btnShort']))?'http://'.ADRESS.'/'.$urlMini:PLEASE_ENTER_A_LONG_URL; ?>'
			onfocus="if (this.value == '<?php echo PLEASE_ENTER_A_LONG_URL; ?>') {this.value = '';}"
			onblur="if (this.value == '') {this.value = '<?php echo PLEASE_ENTER_A_LONG_URL; ?>';}">
			<br>
			<input type="submit" name="btnShort" id="btnShort" value="<?php echo BTN_SHORT; ?>">
			<?php
				if($isFF = strstr($_SERVER['HTTP_USER_AGENT'], 'Firefox')) {
					echo '<input type="submit" id="btnCopy" value="'.BTN_COPY.'">';
				}
			?>
			
		</form>
		<?php
			if(!empty($errMess)) {
				echo '<div id="mess">'.$errMess.'</div>';
			}
			
			$total = $db->selectVal('SELECT COUNT(*) AS total FROM ' . $prefix . 'urls;');
			echo '<p>'.THERE_IS_CURRENTLY.$total['total'].LINKS_IN_THE_DATABASE.'</p><br/>'.SITE_INFO;
			
			if($isFF) {
				echo '
		<p><a href="#" id="btnHelp">'.HOW_TO_GET_COPY_TO_CLIPBOARD_WITH_FIREFOX.'</a></p>
		<div id="help">'.HELP_CONTENT.'</div>
		';
			}
		?>
		
		<script type="text/javascript">
			$(function() {
				$("#btnShort").click(function() {
					var frmValide = true;
					if(!$("#txtUrlOrg").val().match(/(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/)) {
						frmValide = false;
						alert("<?php echo INVALID_ADRESS; ?>");
					}
					return frmValide;
				});
	
				$("#btnCopy").click(
					function copyToClipboardFF(sText) {
						try {
							// Test if clipboard is accessible
							netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
						} catch(e) {}
						var gClipboardHelper =
							Components.classes["@mozilla.org/widget/clipboardhelper;1"]
							.getService(Components.interfaces.nsIClipboardHelper);
						// Copy text into clipboard
						gClipboardHelper.copyString(sText);
						// We don't want to follow link after clic
						return false;
					}
				);
				
				$("#btnHelp").click(function () {
					$("#help").slideToggle("slow");
				});
			});
		</script>
<?php echo showFooter(); ?>
