reurl
=====

![reurl logo](https://bytebucket.org/satbadkd/reurl/raw/29d3e29aacfeadd0b850650b714e9f85442f4dad/themes/default/images/2rightarrow.png)

Copyright (c) 2014 Pierre Duchemin <pierre-duchemin@gmx.com>.
Dual licensed under the [GNU GPLv3+ license](https://www.gnu.org/licenses/gpl-3.0.fr.html)
and the [CeCILL license](http://www.cecill.info/licences/Licence_CeCILL_V2.1-fr.html).
Version 1.0

Description
-----------

URL shortener, useful for microblogging services like Twitter who limit the number of characters in every publication.
Developped with PHP and JQuery. Uses a MySQL database.

It allow me to keep control over my reduced URLs.
This code is running at [reurl.fr.nf](http://reurl.fr.nf). Feel free to contact me for anything about reurl.

---

Raccourcisseur d'URL, utile pour des services comme Twitter qui limitent le nombre de caractère des publications.
Développé avec PHP et JQuery. Utilise une base de données MySQL.

Ce projet me permet de garder le contrôle sur mes adresses url réduites.
Ce code est en production sur [reurl.fr.nf](http://reurl.fr.nf).
N'hésitez pas à me contacter pour tout ce qui concerne reurl.

Installation
------------

* Import reurl.sql on a mysql or mariadb database.
* Replace default values in config.inc.php.
* Modify DB.class.php and set these variables :
	- $PARAM_host : your host (ex: localhost)
	- $PARAM_db_name : your database's name
	- $PARAM_user : a username on your db
	- $PARAM_password : his password

---

* Importez reurl.sql sur une base de donnée mysql ou mariadb.
* Modifiez les valeurs par defaut dans config.inc.php.
* Modifiez DB.class.php et définissez ces variables :
	- $PARAM_host : votre hébergeur (ex: localhost)
	- $PARAM_db_name : le nom de votre base de données
	- $PARAM_user : votre nom d'utilisateur sur la base de données
	- $PARAM_password : votre mot de passe associé

Credits
-------

Some icons by [Yusuke Kamiyamane](http://p.yusukekamiyamane.com/).
Licensed under a [Creative Commons Attribution 3.0 License](http://creativecommons.org/licenses/by/3.0/).
Some others by [Crystal Project Icons](http://www.everaldo.com).
Licenced under [LGPL licence](https://www.gnu.org/licenses/lgpl.html).

Copyright
---------

This program is free software distributed under the terms of two licenses, the
CeCILL license that fits European law, and the GNU General Public
License. You can use, modify and/ or redistribute the software under the terms
of the CeCILL license as circulated by CEA, CNRS and INRIA at the following
URL <http://www.cecill.info> or under the terms of the GNU GPL as published by
the Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL and GPL licenses and that you accept their terms.