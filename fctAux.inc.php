<?php
	require_once 'config.inc.php';
	
	function showHeader() {
		return '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=UTF-8">
		<meta name="description" content="Service web de réduction d\'URL">
		<link rel="Shortcut Icon" href="themes/'.THEME.'/images/favicon.png" type="image/png">
		<link rel="stylesheet" href="themes/'.THEME.'/styles.css" type="text/css">
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<title>'.ADRESS.' - '.SUBTITLE.'</title>
	</head>
	<body>
		<div id="square">
		<div id="transparency"></div>
			<h1 id="title">'.ADRESS.'</h1>
			<noscript>'.THIS_WEBSITE_REQUIRES_JAVASCRIPT.'<br><br></noscript>
';
	}
	
	function showFooter() {
		return '
		</div>
	</body>
</html>';
	}
?>